from sklearn.ensemble import RandomForestClassifier


def get_random_forest(model='sae'):
    thresholds = {
        'roc': 0.5,
        'pr': 0.5
    }
    if 'engineered' in model:
        classifier = RandomForestClassifier(
            n_estimators=512, criterion='gini', max_depth=8, max_features='log2',
            bootstrap=True, class_weight='balanced', random_state=42, n_jobs=-1,
            max_samples=0.1
        )
        thresholds['roc'] = 0.2206
        thresholds['pr'] = 0.5460
    elif 'tae' in model:
        classifier = RandomForestClassifier(
            n_estimators=512, criterion='gini', max_depth=8, max_features='sqrt',
            bootstrap=True, class_weight='balanced', random_state=42, n_jobs=-1,
            max_samples=0.2
        )
        thresholds['roc'] = 0.2736
        thresholds['pr'] = 0.5071
    elif 'sae' in model:
        classifier = RandomForestClassifier(
            n_estimators=512, criterion='gini', max_depth=8, max_features='sqrt',
            bootstrap=True, class_weight='balanced', random_state=42, n_jobs=-1,
            max_samples=0.2
        )
        thresholds['roc'] = 0.2620
        thresholds['pr'] = 0.4941
    else:
        classifier = None
        thresholds['roc'] = 0.5
        thresholds['pr'] = 0.5

    return classifier, thresholds
