import pandas as pd
from itertools import product


training_strategy_parameters = {
    'lr': [1e-5, 1e-4, 1e-3],
    'batch': [16, 32, 64, 128],
    'epochs': [120]
}

data_parameters = {
    'batch_dist': [.0, .5],
    'aug': [0, 1],
    'epochs': [40]
}

default_training_parameters = {
    'lr': [1e-4],
    'batch': [64],
    'epochs': [120],
}

####################################
###        TMP ABLATIONS         ###
####################################


tae_hyper_parameters = {
    'ks_st_combinations': [(8, 4), (20, 10)],
    'hidden_dim': [2**(-1), 2**0],
    'feature_dim': [32, 64],
    'activation_fct': ['leaky_relu'],
    'batch_dist': [.0, .5],
    'aug': [0, 1]
}

combinations = list(product(
    tae_hyper_parameters['ks_st_combinations'],
    tae_hyper_parameters['hidden_dim'],
    tae_hyper_parameters['feature_dim'],
    tae_hyper_parameters['activation_fct'],
    tae_hyper_parameters['batch_dist'],
    tae_hyper_parameters['aug'],
    default_training_parameters['lr'],
    default_training_parameters['batch'],
    default_training_parameters['epochs']
))

tae_model_parameters_ablation = pd.DataFrame(combinations, columns=list(tae_hyper_parameters.keys()) + list(default_training_parameters.keys()))
tae_model_parameters_ablation['kernel_size'] = tae_model_parameters_ablation['ks_st_combinations'].apply(
    lambda x: x[0]
)
tae_model_parameters_ablation['stride'] = tae_model_parameters_ablation['ks_st_combinations'].apply(
    lambda x: x[1]
)
tae_model_parameters_ablation = tae_model_parameters_ablation.drop(columns='ks_st_combinations')


temporary_tae = pd.DataFrame(
    [
        [1., 32, 20, 10, 'leaky_relu', 0.6, 1]
    ],
    columns=['hidden_dim', 'feature_dim', 'kernel_size', 'stride', 'activation_fct', 'batch_dist', 'aug']
)

tae_hyper_parameters_names = temporary_tae.columns
training_combinations = list(product(
    list(temporary_tae['hidden_dim']),
    list(temporary_tae['feature_dim']),
    list(temporary_tae['kernel_size']),
    list(temporary_tae['stride']),
    list(temporary_tae['activation_fct']),
    list(temporary_tae['batch_dist']),
    list(temporary_tae['aug']),
    training_strategy_parameters['lr'],
    training_strategy_parameters['batch'],
    training_strategy_parameters['epochs']
))
tae_training_parameters_ablation = pd.DataFrame(training_combinations, columns=list(tae_hyper_parameters_names) + list(training_strategy_parameters.keys()))

selected_tae = pd.DataFrame(
    [
        [1e-4, 128, 120, 120, 1., 32, 20, 10, 'leaky_relu', .6, 1]
    ],
    columns=['lr', 'batch', 'epochs', 'extract_features', 'hidden_dim', 'feature_dim', 'kernel_size', 'stride', 'activation_fct', 'batch_dist', 'aug']
)

####################################
###        SAE ABLATIONS         ###
####################################

sae_hyper_parameters = {
    'layers': [2, 3, 4],
    'hidden_dim': [0],
    'feature_dim': [16, 32, 63],
    'activation_fct': ['tanh', 'leaky_relu'],
    'spectral_length': [200, 400]
}

combinations = list(product(
    sae_hyper_parameters['layers'],
    sae_hyper_parameters['hidden_dim'],
    sae_hyper_parameters['feature_dim'],
    sae_hyper_parameters['activation_fct'],
    sae_hyper_parameters['spectral_length'],
    default_training_parameters['lr'],
    default_training_parameters['batch'],
    default_training_parameters['epochs']
))
sae_model_parameters_ablation = pd.DataFrame(combinations, columns=list(sae_hyper_parameters.keys()) + list(default_training_parameters.keys()))

temporary_sae = pd.DataFrame(
    [
        [3, 0, 16, 'tanh', 200]
    ],
    columns=['layers', 'hidden_dim', 'feature_dim', 'activation_fct', 'spectral_length']
)

sae_hyper_parameters_names = temporary_sae.columns
data_combinations = list(product(
    list(temporary_sae['layers']),
    list(temporary_sae['hidden_dim']),
    list(temporary_sae['feature_dim']),
    list(temporary_sae['activation_fct']),
    list(temporary_sae['spectral_length']),
    data_parameters['batch_dist'],
    data_parameters['aug'],
    data_parameters['epochs']
))
sae_data_parameters_ablation = pd.DataFrame(data_combinations, columns=list(sae_hyper_parameters_names) + list(data_parameters.keys()))

temporary_sae = pd.DataFrame(
    [
        [3, 0, 16, 'tanh', 200, 0.5, 0]
    ],
    columns=['layers', 'hidden_dim', 'feature_dim', 'activation_fct', 'spectral_length', 'batch_dist', 'aug']
)

sae_hyper_parameters_names = temporary_sae.columns
training_combinations = list(product(
    list(temporary_sae['layers']),
    list(temporary_sae['hidden_dim']),
    list(temporary_sae['feature_dim']),
    list(temporary_sae['activation_fct']),
    list(temporary_sae['spectral_length']),
    list(temporary_sae['batch_dist']),
    list(temporary_sae['aug']),
    training_strategy_parameters['lr'],
    training_strategy_parameters['batch'],
    training_strategy_parameters['epochs']
))
sae_training_parameters_ablation = pd.DataFrame(training_combinations, columns=list(sae_hyper_parameters_names) + list(training_strategy_parameters.keys()))

selected_sae = pd.DataFrame(
    [
        [1e-4, 128, 10, 5, 3, 0, 16, 'tanh', 200, 0.5, 0]
    ],
    columns=['lr', 'batch', 'epochs', 'extract_features', 'layers', 'hidden_dim', 'feature_dim', 'activation_fct', 'spectral_length', 'batch_dist', 'aug']
)
