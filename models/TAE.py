import torch
from torch import nn
from models.TAE_Modules import Encoder, Decoder
import torch.optim as optim


def get_encoder_decoder(in_channels, kernel_size, stride, hidden_dim, feature_dim,
                        activation_fct, ts_len=2000, device=None):
    encoder = Encoder(in_channels=in_channels, kernel_size=kernel_size, stride=stride,
                      hidden_dim=hidden_dim, feature_dim=feature_dim,
                      activation_fct=activation_fct, ts_len=ts_len, device=device)
    filters = encoder.filters
    target_lengths = encoder.layer_lengths
    padding = encoder.padding
    decoder = Decoder(in_channels=in_channels, kernel_size=kernel_size, stride=stride,
                      filters=filters, feature_dim=feature_dim, target_lengths=target_lengths,
                      padding=padding, activation_fct=activation_fct, ts_len=ts_len, device=device)
    return encoder, decoder


class TAE(nn.Module):
    def __init__(self,
                 hidden_dim=1.,
                 feature_dim=32,
                 kernel_size=20,
                 stride=10,
                 activation_fct='leaky_relu',
                 ts_len=2000,
                 learning_rate=0.001,
                 device=None):
        super(TAE, self).__init__()

        self.layer = None
        in_channels = 1
        self.feature_dim = feature_dim

        self.encoder, self.decoder = get_encoder_decoder(
            in_channels, kernel_size, stride, hidden_dim, feature_dim, activation_fct, ts_len=ts_len
        )

        self.criterion = nn.MSELoss()

        parameters = list(self.encoder.parameters()) + list(self.decoder.parameters())
        self.optimizer = optim.Adam(parameters, lr=learning_rate)

        if torch.cuda.is_available():
            self.encoder.cuda(device)
            self.decoder.cuda(device)
            self.criterion.cuda(device)

    def unwrap_sample(self, sample):
        input = sample['temporal input']
        target = input
        return input, target

    def forward(self, sample):
        x, target = self.unwrap_sample(sample)

        feat = self.encoder(x)
        decoder_input = feat.unsqueeze(-1).repeat(1, 1, self.encoder.latent_length)

        x_hat = self.decoder(decoder_input)

        return x_hat, feat, target

    def fit(self, sample):
        reconstruction, features, target = self.forward(sample)

        loss = self.criterion(reconstruction, target=target)

        # compute gradient and do update step
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

        return loss

    def predict(self, sample):
        reconstruction, features, target = self.forward(sample)

        loss = self.criterion(reconstruction, target=target)

        return loss, reconstruction, features, target
