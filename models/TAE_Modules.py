import torch
from torch import nn


def get_out_length(l_in, kernel_size, stride, padding, dilation=1, output_padding=0):
    return (l_in - 1) * stride - 2 * padding + dilation * (kernel_size - 1) + output_padding + 1


def get_output_padding(ts_len, output_length_encoder_layers, kernel_size, stride, padding):
    tar_ = output_length_encoder_layers[1:] + [ts_len]
    n = len(tar_)
    lin = [output_length_encoder_layers[0]]
    is_state = []
    for layer in range(len(tar_)):
        is_state.append(get_out_length(lin[layer], kernel_size, stride, padding))
        lin.append(is_state[layer])
    differences = []
    for i, tar in enumerate(list(tar_)):
        diff = int(tar - is_state[i])
        differences.append(diff)
        if diff:
            is_state[i] = is_state[i] + diff
            tmp = []
            l_in_tmp = is_state[i]
            for j in range(n - (i + 1)):
                tmp.append(get_out_length(l_in_tmp, kernel_size, stride, padding))
                l_in_tmp = tmp[j]
            is_state[i + 1:] = tmp
    output_padding = differences
    return output_padding


class LSTM(nn.Module):
    def __init__(self, in_channels, out_channels, type='output', bidirectional=False, device=None):
        super(LSTM, self).__init__()
        self.in_channels = in_channels
        self.lstm = nn.LSTM(input_size=in_channels, hidden_size=out_channels, batch_first=True, bidirectional=bidirectional)
        self.type = type
        if torch.cuda.is_available():
            self.lstm.cuda(device)

    def forward(self, x):
        # input x is in shape: (batch_size, channels, length)
        if x.shape[1] == self.in_channels:
            x = torch.transpose(x, 1, 2)
        # out: [bs, len, dim]
        # h_n: [lay, bs, dim]
        out, (h_n, _) = self.lstm(x)
        out_seq = torch.transpose(out, 1, 2)
        h_n_out = h_n[-1, :, :]
        return h_n_out if self.type == 'output' else out_seq


class Encoder(nn.Module):
    def __init__(self, in_channels, kernel_size, stride, hidden_dim, feature_dim, activation_fct='leaky_relu', ts_len=2000, device=None):
        super(Encoder, self).__init__()
        if activation_fct == 'leaky_relu':
            self.activation_fct = nn.LeakyReLU()
        elif activation_fct == 'tanh':
            self.activation_fct = nn.Tanh()
        else:
            self.activation_fct = nn.ReLU()
        self.feature_dim = feature_dim

        ks = kernel_size
        st = stride
        st_lay_map = {
            2: (8, 1, 1),  # max 1 for 128 channels
            4: (4, 16, 2),  # max 16 for 128 channels
            6: (3, 32, 3),  # max 32 for 128 channels
            8: (3, 32, 4),  # max 32 for 128 channels
            10: (3, 32, 5),  # max 32 for 128 channels
        }
        layers = st_lay_map[stride][0]
        pd = st_lay_map[stride][2]
        self.padding = pd
        hd = int(hidden_dim * st_lay_map[stride][1] if st_lay_map[stride][1] > 1 else 1)
        self.filters = [2 ** i * hd for i in range(layers)]

        modules = []
        ch = in_channels
        for layer, channels in enumerate(self.filters):
            modules.append(
                nn.Sequential(
                    nn.Conv1d(ch, out_channels=channels,
                              kernel_size=ks, stride=st, padding=pd),
                    nn.BatchNorm1d(channels),
                    self.activation_fct
                )
            )
            ch = channels
        modules.append(
            nn.Sequential(
                LSTM(in_channels=self.filters[-1], out_channels=feature_dim, type='output', bidirectional=False,
                     device=device)
            )
        )
        self.encoder = nn.Sequential(*modules)

        out_len = []
        y = torch.ones((10, in_channels, ts_len))
        if torch.cuda.is_available():
            self.encoder.cuda(device)
            y = y.cuda(device)
        for layer in range(layers+1):
            y = self.encoder[layer](y)
            out_len.append(y.shape[-1])
        self.olen = out_len
        out_len.reverse()
        self.layer_lengths = out_len[1:]
        self.latent_length = self.layer_lengths[0]

        if torch.cuda.is_available():
            self.encoder.cuda(device)

    def forward(self, x):
        return self.encoder(x)


class Decoder(nn.Module):
    def __init__(self, in_channels, kernel_size, stride, filters, feature_dim, target_lengths, padding,
                 activation_fct='leaky_relu', ts_len=2000, device=None):
        super(Decoder, self).__init__()
        self.latent_length = target_lengths[0]
        if activation_fct == 'leaky_relu':
            self.activation_fct = nn.LeakyReLU()
        elif activation_fct == 'tanh':
            self.activation_fct = nn.Tanh()
        else:
            self.activation_fct = nn.ReLU()

        ks = kernel_size
        st = stride
        pd = padding
        output_padding = get_output_padding(ts_len, target_lengths, ks, st, pd)
        self.opad = output_padding

        modules = []
        ch = feature_dim
        n_channels = filters[::-1][0:]
        for layer, channels in enumerate(n_channels):
            modules.append(
                nn.Sequential(
                    nn.ConvTranspose1d(ch, out_channels=channels, kernel_size=ks, stride=st, padding=pd,
                                       output_padding=output_padding[layer]),
                    nn.BatchNorm1d(channels),
                    self.activation_fct
                )
            )
            ch = channels

        modules.append(
            nn.Sequential(
                nn.Conv1d(n_channels[-1], out_channels=in_channels, kernel_size=3, stride=1, padding=1),
                nn.Tanh()
            )
        )
        self.decoder = nn.Sequential(*modules)

        if torch.cuda.is_available():
            self.decoder.cuda(device)

    def forward(self, x):
        return self.decoder(x)
