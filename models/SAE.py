import torch
import torch.nn as nn
import numpy as np
import torch.optim as optim


class SAE(nn.Module):
    def __init__(self,
                 feature_dim=16,
                 in_channels=1,
                 layers=3,
                 hidden_dim=0,
                 n_freqs=200,
                 activation_fct='tanh',
                 learning_rate=0.001,
                 device=None):
        super(SAE, self).__init__()
        self.n_layers = layers
        self.n_channels = in_channels
        hidden_dim = int(hidden_dim)

        self.input_length = n_freqs
        self.in_channels = 1
        self.feature_dim = feature_dim
        self.activation_fct = nn.LeakyReLU() if activation_fct == 'leaky_relu' else nn.Tanh()

        if hidden_dim != 0:
            layer_dim = [self.input_length] + [hidden_dim]
            encoding_steps = self.n_layers - 1
        else:
            layer_dim = [self.input_length]
            encoding_steps = self.n_layers

        reduction = int(np.floor((layer_dim[-1]-feature_dim)/encoding_steps))
        tmp = layer_dim[-1]
        for _ in range(encoding_steps-1):
            tmp -= reduction
            layer_dim.append(tmp)
        layer_dim = layer_dim + [self.feature_dim]

        # ENCODER
        modules = []
        for i in range(self.n_layers):
            modules.append(
                nn.Sequential(
                    nn.Linear(in_features=layer_dim[i], out_features=layer_dim[i+1]),
                    nn.LayerNorm(layer_dim[i+1]),
                    self.activation_fct
                )
            )
        self.encoder = nn.Sequential(*modules)

        # DECODER
        modules = []
        for i in range(layers):
            modules.append(
                nn.Sequential(
                    nn.Linear(in_features=layer_dim[layers-i], out_features=layer_dim[layers-(i + 1)]),
                    nn.LayerNorm(layer_dim[layers-(i + 1)]) if i+1 != layers else nn.Identity(),
                    self.activation_fct
                )
            )
        self.decoder = nn.Sequential(*modules)

        self.criterion = nn.MSELoss()

        parameters = list(self.encoder.parameters()) + list(self.decoder.parameters())
        self.optimizer = optim.Adam(parameters, lr=learning_rate)

        if torch.cuda.is_available():
            self.encoder.cuda(device)
            self.decoder.cuda(device)
            self.criterion.cuda(device)

    def unwrap_sample(self, sample):
        input = sample['spectral input']
        target = input
        return input, target

    def forward(self, sample):
        x, target = self.unwrap_sample(sample)

        z = self.encoder(x)

        x_hat = self.decoder(z)
        return x_hat, z.squeeze(1), target

    def fit(self, sample):
        reconstruction, features, target = self.forward(sample)

        loss = self.criterion(reconstruction, target=target)

        # compute gradient and do update step
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

        return loss

    def predict(self, sample):
        reconstruction, features, target = self.forward(sample)

        loss = self.criterion(reconstruction, target=target)

        return loss, reconstruction, features, target
