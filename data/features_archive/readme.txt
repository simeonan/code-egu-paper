The engineered features are based on the work of Provost et al. (2017).

Reference:
    Provost, F., Hibert, C., & Malet, J. P. (2017). Automatic classification of endogenous landslide seismicity
    using the Random Forest supervised classifier. Geophysical Research Letters, 44(1), 113-120.

Open Source Code:
    Turner, R. J., Latto, R. B., Reading, A. M. (2021). An ObsPy Library for Event Detection and Seismic
    Attribute Calculation: Preparing Waveforms for Automated Analysis. Journal of Open Research Software, 9: 29.
    https://github.com/rossjturner/seismic_attributes/tree/main
