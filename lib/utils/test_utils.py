import random
import scipy
import pandas as pd
import numpy as np
import os
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix, roc_curve, precision_recall_curve, classification_report
from lib.utils.variables import *
from lib.dataset.dataloader import get_data_loaders
from obspy.core import UTCDateTime
from datetime import datetime


class ManualCuts(object):
    def __init__(self, data_type='seismic'):
        cuts_dir = DATA_DIRECTORY + 'manual_cuts/'
        self.manual_cuts = pd.read_csv(cuts_dir + 'manual_cuts_' + data_type + '.csv').drop(columns=['Unnamed: 0'])

    def get(self, event_id):
        selection = self.manual_cuts['Event_id'] == event_id
        date_cuts = self.manual_cuts.loc[selection, :].reset_index().drop(columns=['index'])

        starttimes = date_cuts['starttime']
        time_basis = UTCDateTime(event_id[0:4] + '-' + event_id[4:6] + '-' + event_id[6:8] + 'T00:00:00.000000Z')
        # e.g. 2021-01-28T01:22:00.000000Z
        starttimes = starttimes.apply(lambda x: UTCDateTime(datetime.strptime(x, "%Y-%m-%dT%H:%M:%S.%fZ")))

        # rebase manual cuts on starttime of the day, i.e. 00:00:00
        starttimes = starttimes.apply(lambda x: x - time_basis)

        # e.g. 984000 (index in array)
        spr = 200
        startidx = starttimes.apply(lambda x: x * spr)

        # rebase cut times on start of the day
        date_cuts['initial_time'] = date_cuts['initial_time'] + startidx
        date_cuts['end_time'] = date_cuts['end_time'] + startidx

        date_cuts['initial_time'] = date_cuts['initial_time'].apply(
            lambda x: int(np.floor(x))
        )
        date_cuts['end_time'] = date_cuts['end_time'].apply(
            lambda x: int(np.ceil(x))
        )

        return date_cuts.loc[0, 'initial_time'], date_cuts.loc[0, 'end_time']


def get_features(data_dir, model='sae'):
    features = {
        'train': pd.read_csv(data_dir + model + '_train_features.csv').drop(columns=['Unnamed: 0']),
        'test': pd.read_csv(data_dir + model + '_test_features.csv').drop(columns=['Unnamed: 0'])
    }
    feature_names = [name for name in features['test'].columns if name not in META_COLUMNS]
    features['names'] = feature_names
    return features


def extract_encoder_features(model, saved_args, split='train', data_dir=''):
    train_dataloader, validation_dataloader, _ = get_data_loaders(
        saved_args, k=saved_args.fold, batch_size=1, shuffle=False, feature_ext=True
    )
    if split == 'train':
        dataloader = train_dataloader
        filename = saved_args.model + '_train_features.csv'
    else:
        dataloader = validation_dataloader
        filename = saved_args.model + '_test_features.csv'

    path = data_dir + 'features/'
    if not os.path.exists(path):
        os.makedirs(path)

    dim = saved_args.feature_dim

    encoder_attributes = ['m' + str(i+1) for i in range(dim)]
    feature_container = []

    model.eval()
    with torch.no_grad():
        for i, sample in enumerate(dataloader):
            dataset_id = sample['id']

            output, features, target = model(sample)

            features = features.cpu().detach().numpy()

            id_df = dataloader.dataset.data.loc[dataset_id, META_COLUMNS]
            identifier_dict = {
                'Event_id': id_df['Event_id'].values[0],
                'Component': id_df['Component'].values[0],
                'initial_time': id_df['initial_time'].values[0],
                'end_time': id_df['end_time'].values[0],
                'label': id_df['label'].values[0],
                'av_score': id_df['av_score'].values[0],
                'eq_score': id_df['eq_score'].values[0]
            }
            features_dict = {
                feat_name: features[0, i] for i, feat_name in enumerate(encoder_attributes)
            }
            data = {**identifier_dict, **features_dict}

            feature_container.append(data)

    pd.DataFrame(feature_container).to_csv(path + filename)


def cm_analysis(y_true, y_pred, filename='', ymap=None, figsize=(4, 6), epoch=0, binary=False,
                writer_dict=None, cv_fold=None, tensorboard_add_on='', format='png', model_id=''):
    if 'seismic' in model_id or 'engineered' in model_id:
        title = 'Seismic Attribute Classification'
    elif 'tae' in model_id:
        title = 'TAE Feature Classification'
    elif 'sae' in model_id:
        title = 'SAE Feature Classification'
    else:
        title = ''
    if binary:
        labels = [1, 0]
        labels_dict = {1: 'Avalanche', 0: 'Noise'}
    else:
        labels = [1, 2, 0]
        labels_dict = {1: 'Avalanche', 2: 'Earthquake', 0: 'Noise'}
    label_names = list(labels_dict.values())
    if ymap is not None:
        y_pred = [ymap[yi] for yi in y_pred]
        y_true = [ymap[yi] for yi in y_true]
        labels = [ymap[yi] for yi in labels]
    cm = confusion_matrix(y_true, y_pred, labels=labels)
    cm_sum = np.sum(cm, axis=1, keepdims=True)
    cm_perc = cm / cm_sum.astype(float) * 100
    annot = np.empty_like(cm).astype(str)
    nrows, ncols = cm.shape
    for i in range(nrows):
        for j in range(ncols):
            c = cm[i, j]
            p = cm_perc[i, j]
            if c == 0:
                annot[i, j] = ''
            else:
                annot[i, j] = '%.1f%%\n%d' % (p, c)
    cm = pd.DataFrame(cm_perc, index=label_names, columns=label_names)
    cm.index.name = 'True Label'
    cm.columns.name = 'Predicted Label'

    if writer_dict is None:
        rc = {'figure.figsize': figsize, 'figure.dpi': 600, 'font.size': 14, 'axes.labelsize': 14,
              'axes.titlesize': 14, 'xtick.labelsize': 14, 'ytick.labelsize': 14}
        sns.set_theme(style="ticks", color_codes=True, rc=rc)
    fig, ax = plt.subplots(figsize=figsize)
    sns.heatmap(cm, annot=annot, fmt='', ax=ax, cmap='PuBu', cbar=False, square=True, linewidths=1)
    ax.set_title(title)
    if writer_dict:
        writer = writer_dict['writer']
        if cv_fold is not None:
            writer.add_figure(f'Fold_{cv_fold}/Confusion_Matrix_of_Epoch_{epoch}' + tensorboard_add_on,
                              figure=ax.get_figure())
        else:
            writer.add_figure(f'Confusion_Matrix_of_Epoch_{epoch}' + tensorboard_add_on, figure=ax.get_figure())
        writer.flush()
    elif filename != '':
        if format == 'png':
            plt.savefig(filename, bbox_inches='tight')
        else:
            plt.savefig(filename, bbox_inches='tight', format=format)
    else:
        plt.show()
    plt.close()


def plot_expert_ratings_over_output_probabilities(predictions, save_dir=None, model_name='', filename=''):
    if 'engineered' in model_name:
        title = 'Seismic Attributes'
    elif 'tae' in model_name:
        title = 'TAE Features'
    elif 'sae' in model_name:
        title = 'SAE Features'
    else:
        title = ''
    # avalanches = predictions.loc[predictions['av_score'] > 0, :]
    scores = [0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0]
    data = {}
    for score in scores:
        data[str(score)] = predictions.loc[predictions['av_score'] == score, 'P_av']

    plt.rcParams.update({'font.size': 20})
    fig, ax = plt.subplots()
    fig.set_figheight(8)
    fig.set_figwidth(8)
    ax.boxplot(list(data.values()), labels=list(data.keys()))
    ax.vlines(4.5, 0, 1, colors='royalblue', linestyles='dotted')
    plt.xlabel('Expert Avalanche Score')
    plt.ylabel('RF Output Probability')
    plt.title(title)
    if save_dir is not None:
        plt.savefig(save_dir + filename + '.png',
                    bbox_inches='tight')
    else:
        plt.show()
    plt.close()


def consecutive_window_agreement(lst, window_size=3):
    lst = [int((p > 0.5)) * 1 for p in lst]
    if len(lst) >= window_size:
        out = (window_size in [sum(lst[i:i+window_size]) for i in range(len(lst) - window_size + 1)]) * 1
    else:
        out = scipy.stats.mode(lst)[0]
    return out


def probabilistic_ensembling(df, aggregation_function='mean'):
    probabilities = ['P_noise', 'P_av']

    df[aggregation_function + '_maj'] = df[probabilities].apply(
        lambda row: row.argmax(), axis=1)
    df.reset_index(inplace=True)


def ensemble_predictions(window_predictions, n_win=2):
    probabilities = [name for name in list(window_predictions.columns) if 'P_' in name]

    window_predictions['mode_maj'] = window_predictions['Yhat']

    aggregation_function = 'mean'
    p_agg = dict(
        zip(probabilities, len(probabilities)*[aggregation_function])
    )

    array_predictions = window_predictions.groupby(
        ['initial_time']
    ).agg(
        {
            **{
                'Event_id': lambda x: list(x)[0],
                'Ytrue': lambda x: list(x)[0],
                'av_score': lambda x: list(x)[0],
                'eq_score': lambda x: list(x)[0],
                'mode_maj': lambda x: scipy.stats.mode(x)[0]
            },
            **p_agg
        }
    )

    probabilistic_ensembling(array_predictions)

    array_predictions['consecutive_' + str(n_win)] = array_predictions['P_av']

    event_predictions = array_predictions.groupby(
        ['Event_id']
    ).agg(
        {
            **{
                'initial_time': lambda x: list(x)[0],
                'Ytrue': lambda x: list(x)[0],
                'av_score': lambda x: list(x)[0],
                'eq_score': lambda x: list(x)[0],
                'mode_maj': lambda x: scipy.stats.mode(x)[0],
                'consecutive_' + str(n_win): lambda x: consecutive_window_agreement(list(x), n_win)
            },
            **p_agg
        }
    )

    probabilistic_ensembling(event_predictions)

    return array_predictions, event_predictions


def reformat_clf_report(clf_report):
    metrics = {'precision': [], 'recall': [], 'f1-score': []}
    accuracy = {'accuracy': 0}
    metric_keys = [key for key, _ in metrics.items()]
    for key, value in clf_report.items():
        if type(value) is not dict:
            accuracy['accuracy'] = value
            continue
        for key in metric_keys:
            metrics[key].append(value[key])
    report = {**accuracy, **metrics}
    return report


def save_report(model_id, report, metrics_dict, split, save_dir, binary=False):
    new_report = reformat_clf_report(report)
    values = list()
    for key, value in new_report.items():
        if type(value) is not list:
            values += [round(value, 4)]
            continue
        values += [round(val, 4) for val in value]
    metrics = [round(value, 4) for _, value in metrics_dict.items()]
    metric_names = [key for key, _ in metrics_dict.items()]
    if binary:
        file_header = ['Model'] + metric_names + ['acc.avg'] \
                      + ['prec.noise', 'prec.avalanche', 'prec.avg', 'prec.wavg'] \
                      + ['rec.noise', 'rec.avalanche', 'rec.avg', 'rec.wavg'] \
                      + ['f1.noise', 'f1.avalanche', 'f1.avg', 'f1.wavg'] \
                      # + ['threshold.roc'] + ['max-gmean'] + ['threshold.pr'] + ['max-fscore']
    else:
        file_header = ['Model'] + ['acc.avg'] \
                      + ['prec.noise', 'prec.avalanche', 'prec.earthquake', 'prec.avg', 'prec.wavg'] \
                      + ['rec.noise', 'rec.avalanche', 'rec.earthquake', 'rec.avg', 'rec.wavg'] \
                      + ['f1.noise', 'f1.avalanche', 'f1.earthquake', 'f1.avg', 'f1.wavg']

    row = np.array([model_id] + metrics + values)
    row = row[np.newaxis, :]

    df = pd.DataFrame(row, columns=file_header)
    df = df.reset_index()
    df = df.drop(columns=['index'])

    df.to_csv(save_dir + split + '_metrics.csv')
    return 0


def save_analysis(save_dir, model_id, y_true, y_hat, clustering_metrics=None, split='val', binary=False, format='png'):
    report_val = classification_report(y_true=y_true, y_pred=y_hat, output_dict=True)

    tn, fp, fn, tp = confusion_matrix(y_true, y_hat).ravel()
    sensitivity = tp / (tp + fn)
    specificity = tn / (tn + fp)
    gmean = np.sqrt(sensitivity * specificity)
    metrics_dict = {
        'sensitivity': sensitivity,
        'specificity': specificity,
        'gmean': gmean
    }
    if clustering_metrics is not None:
        add_metrics = {**metrics_dict, **clustering_metrics}
    else:
        add_metrics = metrics_dict

    filename = save_dir + split + '_confusion_matrix'
    cm_analysis(y_true=y_true, y_pred=y_hat, filename=filename,
                ymap=None, figsize=(5, 4), binary=binary, format=format, model_id=model_id)

    save_report(model_id, report=report_val, metrics_dict=add_metrics, split=split, save_dir=save_dir, binary=binary)


def optimize_threshold(prob_y, test_y, save_dir, plot=True):
    prob_y = prob_y[:, 1]
    # calculate roc curves
    fpr, tpr, thresholds = roc_curve(test_y, prob_y)
    # calculate the g-mean for each threshold
    gmeans = (tpr * (1 - fpr))**0.5
    # locate the index of the largest g-mean
    ix = np.argmax(gmeans)
    best_threshold_roc = thresholds[ix]
    gmean = gmeans[ix]

    if plot:
        # plot the roc curve for the model
        plt.plot([0, 1], [0, 1], linestyle='--', label='No Skill')
        plt.plot(fpr, tpr, marker='.', label='Random Forest')
        plt.scatter(fpr[ix], tpr[ix], marker='o', color='black', label='Best')
        # axis labels
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.legend()
        # show the plot
        plt.savefig(save_dir + 'ROC-Curve.png')
        plt.close()

    precision, recall, thresholds = precision_recall_curve(test_y, prob_y)
    # convert to f score
    fscores = (2 * precision * recall) / (precision + recall)
    fscores = np.nan_to_num(fscores, nan=0.0)
    # locate the index of the largest f score
    ix = np.argmax(fscores)
    best_threshold_pr = thresholds[ix]
    fscore = fscores[ix]

    # plot the roc curve for the model
    if plot:
        no_skill = len(test_y[test_y == 1]) / len(test_y)
        plt.plot([0, 1], [no_skill, no_skill], linestyle='--', label='No Skill')
        plt.plot(recall, precision, marker='.', label='Random Forest')
        plt.scatter(recall[ix], precision[ix], marker='o', color='black', label='Best')
        # axis labels
        plt.xlabel('Recall')
        plt.ylabel('Precision')
        plt.legend()
        # show the plot
        plt.savefig(save_dir + 'Precision-Recall-Curve.png')
        plt.close()

    if plot:
        threshold_df = pd.DataFrame([['roc', best_threshold_roc, gmean],
                                     ['prec-rec', best_threshold_pr, fscore]],
                                    columns=['Type', 'Threshold', 'Scorer (G-Mean, F-Score)'])
        threshold_df.to_csv(save_dir + 'thresholding.csv')
    return best_threshold_roc, best_threshold_pr, gmean, fscore
