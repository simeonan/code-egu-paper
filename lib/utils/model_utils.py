from models.TAE import TAE
from models.SAE import SAE
from lib.utils.logger import *


def load_model(args, model_id, pretrained=False, model_log_dir='', epoch=20, device=None):
    if args.model == 'tae':
        model = TAE(hidden_dim=args.hidden_dim, feature_dim=args.feature_dim,
                    kernel_size=args.kernel_size, stride=args.stride,
                    activation_fct=args.activation_fct, learning_rate=args.lr,
                    ts_len=args.tdim, device=device)
    elif args.model == 'sae':
        model = SAE(feature_dim=args.feature_dim, layers=args.layers,
                    hidden_dim=args.hidden_dim, activation_fct=args.activation_fct,
                    learning_rate=args.lr, n_freqs=args.fdim, device=device)
    else:
        model = None

    if pretrained:
        model_path = model_log_dir + model_id + '/model_ep' + str(epoch - 1) + '.pth'
        if torch.cuda.is_available():
            model.load_state_dict(torch.load(model_path)['model_state_dict'])
        else:
            model.load_state_dict(torch.load(model_path, map_location=torch.device('cpu'))['model_state_dict'])

    if torch.cuda.is_available():
        model.float()
        model.cuda(device)
    else:
        model.float()

    number_of_para = sum(p.numel() for p in model.parameters() if p.requires_grad)
    parameter_names = []
    for name, param in model.named_parameters():
        if param.requires_grad:
            parameter_names.append(name)
    on_gpu = next(model.parameters()).is_cuda

    return model, number_of_para, len(parameter_names), on_gpu
