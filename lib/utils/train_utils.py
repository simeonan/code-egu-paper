import os
import argparse
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import torch

from sklearn.feature_selection import f_classif
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.metrics import silhouette_score

from lib.utils.test_utils import cm_analysis, optimize_threshold
from lib.utils.variables import *
from models.TAE import TAE
from models.SAE import SAE


def parse_args():
    parser = argparse.ArgumentParser(description='Training mode')
    # training
    parser.add_argument('--model',
                        help='which model to use',
                        default='tae',
                        type=str)
    parser.add_argument('--ts_len',
                        help='seismic window length',
                        default=2000,
                        type=int)
    parser.add_argument('--batch',
                        help='batch size used in training',
                        default=64,
                        type=int)
    parser.add_argument('--epochs',
                        help='how many epochs to train',
                        default=1,
                        type=int)
    parser.add_argument('--loss',
                        help='loss to use for model',
                        default='mse',
                        type=str)
    parser.add_argument('--layers',
                        help='number of layers in model',
                        default=5,
                        type=int)
    parser.add_argument('--hidden_dim',
                        help='dimension of first hidden dimension',
                        default=1.,
                        type=float)
    parser.add_argument('--feature_dim',
                        help='dimensions of latent (feature) space',
                        default=96,
                        type=int)
    parser.add_argument('--frequency_input',
                        help='whether to use fft as input',
                        default=False,
                        type=bool)
    parser.add_argument('--spectral_length',
                        help='input length of fft',
                        default=200,
                        type=int)
    parser.add_argument('--final',
                        help='whether to do inference',
                        default=False,
                        type=bool),
    parser.add_argument('--binary',
                        help='whether to perform binary classification',
                        default=False,
                        type=bool)
    parser.add_argument('--extract_features',
                        help='after which epoch to extract features from test set',
                        default=0,
                        type=int)
    parser.add_argument('--kernel_size',
                        help='kernel size in convolutions (tae)',
                        default=3,
                        type=int)
    parser.add_argument('--stride',
                        help='stride in convolutions (tae)',
                        default=2,
                        type=int)
    parser.add_argument('--lr',
                        help='learning rate of optimizer',
                        default=1e-4,
                        type=float)
    parser.add_argument('--kfold_cv',
                        help='whether to perform cross-validation',
                        default=0,
                        type=int)
    parser.add_argument('--fold',
                        help='which cross-validation fold to test on',
                        default=0,
                        type=int)
    parser.add_argument('--activation_fct',
                        help='activation function used in model',
                        default='relu',
                        type=str)
    parser.add_argument('--batch_dist',
                        help='expected portion of class 1 per mini-batch',
                        default=0.,
                        type=float)
    parser.add_argument('--aug',
                        help='whether to augment the input data',
                        default=0,
                        type=int)
    parser.add_argument('--data_type',
                        help='whether to use seimic or infrasound input data',
                        default='seismic',
                        type=str)

    args = parser.parse_args()

    return args


class Container(object):
    def __init__(self):
        self.container = None

    def add(self, x):
        self.container = x if self.container is None else torch.cat((self.container, x), dim=0)

    def get(self):
        return self.container.cpu().detach().numpy()


class Plotter(object):
    def __init__(self, binary):
        self.class_labels = [1, 0] if binary else [1, 2, 0]
        self.event_plotted = dict(zip(self.class_labels, [False for _ in range(len(self.class_labels))]))
        self.recon_plots, self.other_plots, self.tar_plots = {}, {}, {}

    def reset(self):
        self.event_plotted = dict(zip(self.class_labels, [False for _ in range(len(self.class_labels))]))
        self.recon_plots, self.other_plots, self.tar_plots = {}, {}, {}

    def add_data(self, reconstruction, target, label, other=None):
        for lab in self.class_labels:
            if (label == lab).any() and not self.event_plotted[lab]:
                label_ = label.detach().cpu().numpy()
                idx = np.argmax((label_ == lab) * 1)
                self.tar_plots[lab] = target.detach().cpu().numpy()[idx, 0, :]
                self.recon_plots[lab] = reconstruction.detach().cpu().numpy()[idx, 0, :]
                if other:
                    self.other_plots[lab] = other.detach().cpu().numpy()[idx, 0, :]
                self.event_plotted[lab] = True

    def plot(self, epoch, writer_dict, cv_fold=None):
        label_names = {1: 'avalanche', 2: 'earthquake', 0: 'noise'}
        n_classes = len(self.class_labels)
        if len(self.tar_plots[0].shape) == 2:
            two_dimensional = True
        else:
            two_dimensional = False

        if two_dimensional:
            fig, axs = plt.subplots(nrows=n_classes, ncols=2)
            fig.set_figheight(4 * n_classes)
            fig.set_figwidth(4 * n_classes)
            fig.tight_layout()

            i = 0
            for lab in self.class_labels:
                axs[i, 0].imshow(self.tar_plots[lab], origin='lower')
                axs[i, 1].imshow(self.recon_plots[lab], origin='lower')
                axs[i, 0].set_title('Input ' + label_names[lab])
                axs[i, 1].set_title('Reconstruction of ' + label_names[lab])
                i += 1
        else:
            fig, axs = plt.subplots(nrows=n_classes, ncols=1)
            fig.set_figheight(4 * n_classes)
            fig.set_figwidth(16)
            fig.tight_layout()

            i = 0
            for lab in self.class_labels:
                axs[i].plot(self.tar_plots[lab], color='grey')
                axs[i].plot(self.recon_plots[lab], color='orange')
                if bool(self.other_plots):
                    axs[i].plot(self.recon_plots[lab] + self.other_plots[lab], color='lightblue', linestyle='--')
                    axs[i].plot(self.recon_plots[lab] - self.other_plots[lab], color='lightblue', linestyle='--')
                axs[i].set_title('Reconstruction of ' + label_names[lab])
                i += 1

        if writer_dict:
            writer = writer_dict['writer']
            if cv_fold is not None:
                writer.add_figure(tag=f'Fold_{cv_fold}/Reconstructions_of_Epoch_{epoch}', figure=fig)
            else:
                writer.add_figure(tag=f'Test_Fold/Reconstructions_of_Epoch_{epoch}', figure=fig)
        else:
            plt.show()
        plt.close()


def get_latent_space(model, features, labels):
    f_statistic, p_values = f_classif(features, labels)
    f_statistic = 1 / sum(f_statistic) * f_statistic
    feature_importances = [(feat_idx, f_val) for feat_idx, f_val in enumerate(f_statistic)]
    sorted_importance = sorted(feature_importances, key=lambda x: x[1], reverse=True)
    sorted_features = [importance[0] for importance in sorted_importance][:5]

    names = np.array(['m' + str(i + 1) for i in range(model.feature_dim)])[sorted_features]
    features = pd.DataFrame(features[:, sorted_features], columns=list(names))
    labels = pd.DataFrame(labels, columns=['label'])
    latent_space_data = pd.concat((features, labels), axis=1)
    return latent_space_data


def plot_latentspace(data_pp, writer_dict, epoch, hard_labels=True, cv_fold=None):
    plt.figure()
    sns.set(style="ticks", color_codes=True)
    if hard_labels:
        palette = sns.xkcd_palette(['grey', 'blue'])
        if 'av_score' in data_pp.columns:
            data_pp = data_pp.drop(columns=['av_score'])
        pp_plt = sns.pairplot(data_pp, hue='label', palette=palette)
    else:
        data_pp = data_pp.drop(columns=['label'])
        # data_pp['events'] = data_pp['av_score'].apply(lambda x: 0 if (x < 1.5) else x)
        # color_list = ['grey', 'light blue', 'mid blue', 'true blue', 'midnight blue']
        data_pp['events'] = data_pp.apply(
            lambda x: 1 if (x['av_score'] >= 2.) else 2 if (x['eq_score'] >= 2.) else 0, axis=1
        )
        color_list = ['grey', 'blue', 'orange']

        color_list = ['grey', 'light blue', 'mid blue', 'true blue', 'midnight blue']
        palette = sns.xkcd_palette(color_list)
        pp_plt = sns.pairplot(data_pp, hue='events', palette=palette)
    if writer_dict:
        writer = writer_dict['writer']
        if cv_fold is not None:
            writer.add_figure(f'Fold_{cv_fold}/Latent_Space_Visualisation_of_Epoch_{epoch}', pp_plt.figure)
        else:
            writer.add_figure(f'Test_Fold/Latent_Space_Visualisation_of_Epoch_{epoch}', pp_plt.figure)
        writer.flush()
    plt.close()


def classify_features(feat, tar, val_feat, val_tar):
    standard_rf = RandomForestClassifier(
        n_estimators=100,
        criterion='gini',
        max_depth=10,
        max_features='sqrt',
        random_state=42,
        n_jobs=-1,
        class_weight='balanced'
    )

    standard_rf.fit(feat, tar)
    y_hat = standard_rf.predict(val_feat)
    y_hat_val_prob = standard_rf.predict_proba(val_feat)

    feature_dim = feat.shape[-1]
    importances = list(standard_rf.feature_importances_)
    feature_idx = [i for i in range(feature_dim)]
    feature_importances = [(feature, round(importance, 4)) for feature, importance in
                           zip(list(feature_idx), importances)]
    feature_importances = sorted(feature_importances, key=lambda x: x[1], reverse=True)
    sorted_importances = [importance[1] for importance in feature_importances]
    sorted_features = [importance[0] for importance in feature_importances]

    # xpca = PCA(n_components=4).fit_transform(mus)
    idxs = sorted_features[:5]
    xfi = val_feat[:, idxs]
    names = np.array(['m' + str(i + 1) for i in range(feature_dim)])[idxs]
    features_df = pd.DataFrame(xfi, columns=list(names))
    labels_df = pd.DataFrame(val_tar, columns=['label'])
    latentspace_data = pd.concat((features_df, labels_df), axis=1)

    return latentspace_data, y_hat, y_hat_val_prob


def reformat_clf_report(clf_report):
    metrics = {'precision': [], 'recall': [], 'f1-score': []}
    accuracy = {'accuracy': 0}
    metric_keys = [key for key, _ in metrics.items()]
    for key, value in clf_report.items():
        if type(value) is not dict:
            accuracy['accuracy'] = value
            continue
        for key in metric_keys:
            metrics[key].append(value[key])
    report = {**accuracy, **metrics}
    return report


def write_clf_metrics(report, model_name, best_threshold_roc, best_threshold_pr, roc_score, pr_score,
                      sensitivity, specificity, gmean, s_score=0., epoch=0):
    new_report = reformat_clf_report(report)
    values = list()
    for key, value in new_report.items():
        if type(value) is not list:
            values += [round(value, 4)]
            continue
        values += [round(val, 4) for val in value]

    file_header = ['Model'] + ['Sensitivity'] + ['Specificity'] + ['G-Mean'] + ['Silhouette'] + ['Accuracy'] \
                  + ['Precision' for _ in range(4)] + ['Recall' for _ in range(4)] + ['f1-Score' for _ in range(4)] \
                  + ['Threshold ROC'] + ['max G-mean'] + ['Threshold PR'] + ['max F-score']
    data_header = np.array([''] + [''] + [''] + [''] + [''] + ['Average']
                           + 3 * ['Noise', 'Avalanche', 'Average', 'Weighted Avg']
                           + [''] + [''] + [''] + [''])

    row = np.array(
        [model_name]
        + [round(sensitivity, 4)] + [round(specificity, 4)] + [round(gmean, 4)] + [round(s_score, 4)]
        + values
        + [round(best_threshold_roc, 4)] + [round(roc_score, 4)] + [round(best_threshold_pr, 4)] + [round(pr_score, 4)]
    )
    row = row[np.newaxis, :]

    save_dir = PROJECT_DIRECTORY + 'results/'
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)

    if not os.path.exists(save_dir + 'model_comparison_ep' + str(epoch) + '.csv'):
        data_header = data_header[np.newaxis, :]
        df = pd.DataFrame(data_header, columns=file_header)
    else:
        df = pd.read_csv(save_dir + 'model_comparison_ep' + str(epoch) + '.csv').drop(columns=['Unnamed: 0'])
        df.columns = file_header

    row = pd.DataFrame(row, columns=file_header)
    df = pd.concat((df, row), axis=0)
    df = df.reset_index()
    df = df.drop(columns=['index'])
    df.to_csv(save_dir + 'model_comparison_ep' + str(epoch) + '.csv')


def evaluate_classification(train_feat, val_feat, train_lab, val_lab, epoch, model_id, writer_dict, args):
    data_pp, y_hat, y_hat_prob = classify_features(train_feat, train_lab, val_feat, val_lab)
    pred = y_hat
    tar = val_lab

    if args.binary:
        tn, fp, fn, tp = confusion_matrix(tar, pred).ravel()
        sensitivity = tp / (tp + fn)
        specificity = tn / (tn + fp)
        gmean = np.sqrt(sensitivity * specificity)
        s_score = silhouette_score(val_feat, val_lab)
        np.seterr(divide='ignore', invalid='ignore')
        th_roc, th_pr, roc_score, pr_score = optimize_threshold(y_hat_prob, val_lab, save_dir='', plot=False)

        report = classification_report(y_true=val_lab, y_pred=pred, output_dict=True)
        write_clf_metrics(report, model_id, th_roc, th_pr, roc_score, pr_score,
                          sensitivity, specificity, gmean, s_score, epoch=epoch)

    plot_latentspace(data_pp, writer_dict, epoch, hard_labels=True)

    cm_analysis(tar, pred, filename='', ymap=None, figsize=(5, 4),
                epoch=epoch, binary=args.binary, writer_dict=writer_dict)


def get_model(model_name='sae'):
    if model_name == 'sae':
        model = SAE(layers=3, hidden_dim=0, feature_dim=16, activation_fct='tanh')
        path = 'models/log/SAE.pth'
    elif model_name == 'tae':
        model = TAE(kernel_size=20, stride=10, hidden_dim=1., feature_dim=32, activation_fct='leaky_relu')
        path = 'models/log/TAE.pth'
    else:
        model = None
        path = ''

    model.load_state_dict(torch.load(path, map_location=torch.device('cpu'))['model_state_dict'])
    model.float()
    model.eval()

    return model
