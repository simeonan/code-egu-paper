import logging
import os
import time
from torch.utils.tensorboard import SummaryWriter
import pandas as pd
import numpy as np
import torch
from lib.utils.variables import *


class AverageMeter(object):
    """Computes and stores the average and current value"""

    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count if self.count != 0 else 0


class DotDict(dict):
    """dot.notation access to dictionary attributes"""
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__


def get_log_message(epoch, batch, n_batches, speed, batch_time, data_time, train_loss, val_loss=None):
    msg = 'Epoch: [{0}][{1}/{2}]\t' \
          'Time {batch_time.val:.3f}s ({batch_time.avg:.3f}s)\t' \
          'Speed {speed:.1f} samples/s\t' \
          'Data {data_time.val:.3f}s ({data_time.avg:.3f}s)\t' \
          'Train Loss {cur_loss:.5f}\t'\
        .format(epoch, batch, n_batches, batch_time=batch_time,
                speed=speed, data_time=data_time,
                cur_loss=train_loss)
    if val_loss is not None:
        msg += 'Validation Loss: {val_loss:.5f}\t'.format(val_loss=val_loss)
    return msg


def get_dirs(args, time_str):
    model_id = time_str + '_' + args.model

    project_dir = PROJECT_DIRECTORY
    data_dir = DATA_DIRECTORY
    machine = MACHINE

    if args.kfold_cv:
        if args.final:
            train_log_dir = project_dir + 'log/' + 'inference/' + args.model + '/'
            model_log_dir = project_dir + 'model_log/inference/' + args.model + '/' + model_id + '/'
        else:
            train_log_dir = project_dir + 'log/' + 'cv/' + args.model + '/'
            model_log_dir = ''
    else:
        train_log_dir = project_dir + 'log/' + args.model + '/'
        model_log_dir = project_dir + 'model_log/' + args.model + '/' + model_id + '/'
    if not os.path.exists(model_log_dir) and model_log_dir != '':
        os.makedirs(model_log_dir)
    if not os.path.exists(train_log_dir):
        os.makedirs(train_log_dir)

    if torch.cuda.is_available():
        device = torch.cuda.current_device()
    else:
        device = None

    dirs = {
        'project_dir': project_dir,
        'data_dir': data_dir,
        'train_log_dir': train_log_dir,
        'model_log_dir': model_log_dir,
        'model_id': model_id,
        'device': device,
        'machine': machine
    }

    return dirs


def save_args(run_args, project_dir, machine='macbook'):
    args_dict = run_args
    filename = 'args_of_train_runs_on_' + machine + '.csv'
    save_dir = project_dir

    if not os.path.exists(save_dir):
        os.makedirs(save_dir)

    if not os.path.exists(save_dir + filename):
        start_idx = 0
        args_df = pd.DataFrame(args_dict, index=[start_idx])
    else:
        args_df = pd.read_csv(save_dir + filename).drop(columns=['Unnamed: 0'])
        start_idx = len(args_df)
        new = pd.DataFrame(args_dict, index=[start_idx])
        args_df = args_df.append(new, ignore_index=False)
    args_df.to_csv(save_dir + filename)


def create_logger(out_dir, name, phase='train', create_tf_logs=True, fold=None):
    """Create text logger and TensorBoard writer objects

    Args:
        out_dir (str): output directory for saving logs.
        name (str): how to name the tensorboard directory
        phase (str): short description for log, will be appended to log filename.
        create_tf_logs (bool): whether to create TensorBoard writer or not
    Returns:
        logger: text logger
        writer: TensorBoard writer
    """
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    time_str = time.strftime('%Y-%m-%d-%H-%M')
    log_file = '{}_{}.log'.format(time_str, phase)
    final_log_file = os.path.join(out_dir, log_file)
    head = '%(asctime)-15s %(message)s'
    logging.basicConfig(filename=str(final_log_file),
                        format=head)
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    if not logger.hasHandlers():
        console = logging.StreamHandler()
        logging.getLogger('').addHandler(console)

    if fold is not None:
        out_dir += 'fold_' + str(fold) + '/'
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)

    if create_tf_logs:
        try:
            now = time.localtime()
            time_str = time.strftime("%d.%m.%Y-%H:%M:%S", now)
            writer = SummaryWriter(os.path.join(out_dir, f'logs/{name}_{time_str}'))
        except:
            writer = None
    else:
        writer = None

    return logger, writer
