import torch
import numpy as np
import random


SEISMIC_FEATURES = [
    'RappMaxMean', 'RappMaxMedian', 'AsDec', 'KurtoSig', 'KurtoEnv',
    'SkewnessSig', 'SkewnessEnv', 'CorPeakNumber', 'INT1', 'INT2',
    'INT_RATIO', 'ES[0]', 'ES[1]', 'ES[2]', 'ES[3]', 'ES[4]',
    'KurtoF[0]', 'KurtoF[1]', 'KurtoF[2]', 'KurtoF[3]', 'KurtoF[4]',
    'DistDecAmpEnv', 'env_max/duration(Data,sps)', 'MeanFFT', 'MaxFFT',
    'FmaxFFT', 'FCentroid', 'Fquart1', 'Fquart3', 'MedianFFT',
    'VarFFT', 'NpeakFFT', 'MeanPeaksFFT', 'E1FFT', 'E2FFT', 'E3FFT', 'E4FFT',
    'gamma1', 'gamma2', 'gammas', 'SpecKurtoMaxEnv', 'SpecKurtoMedianEnv',
    'RATIOENVSPECMAXMEAN', 'RATIOENVSPECMAXMEDIAN', 'DISTMAXMEAN',
    'DISTMAXMEDIAN', 'NBRPEAKMAX', 'NBRPEAKMEAN', 'NBRPEAKMEDIAN',
    'RATIONBRPEAKMAXMEAN', 'RATIONBRPEAKMAXMED', 'NBRPEAKFREQCENTER',
    'NBRPEAKFREQMAX', 'RATIONBRFREQPEAKS', 'DISTQ2Q1', 'DISTQ3Q2', 'DISTQ3Q1'
]

META_COLUMNS = ['Event_id', 'Component', 'initial_time', 'end_time', 'label', 'av_score', 'eq_score']

DATE_FORMAT = "%Y-%m-%dT%H:%M:%S.%fZ"

SEED = 1
torch.manual_seed(SEED)
random.seed(SEED)
np.random.seed(SEED)

MACHINE = 'local'
ROOT_DIRECTORY = '/Users/andrisimeon/Documents/04_SLF/01_Code/paper-code/code-egu-paper/'
PROJECT_DIRECTORY = ROOT_DIRECTORY
DATA_DIRECTORY = ROOT_DIRECTORY + 'data/'

if torch.cuda.is_available():
    DEVICE = torch.cuda.current_device()
    DEVICE_NAME = torch.cuda.get_device_name(DEVICE)
else:
    DEVICE = None
    DEVICE_NAME = ''

SPLITS = {
    'seismic': {
        'Fold 1': [
            '2021-01-13',
            '2021-01-14',
            '2021-01-15',
            '2021-01-16',
            '2021-01-24',
            '2021-01-25',
            '2021-01-26',
            '2021-01-27',
            '2021-01-28'
        ],
        'Fold 2': [
            '2021-01-29',
            '2021-01-30',
            '2021-01-31',
            '2021-02-01',
            '2021-02-02',
            '2021-02-13',
            '2021-02-14',
            '2021-02-24',
            '2021-03-18',
            '2021-03-30',
            '2021-04-01',
            '2021-04-02',
            '2021-04-28',
            '2021-04-29',
            '2021-04-30',
            '2021-05-01',
            '2021-05-02',
            '2021-05-03',
            '2021-05-11',
            '2021-05-12',
            '2021-05-16',
            '2021-05-20',
            '2021-05-23',
            '2021-05-24'
        ],
        'Fold 3': [
            '2022-01-10',
            '2022-01-11',
            '2022-01-12',
            '2022-01-13',
            '2022-01-14',
            '2022-01-31',
            '2022-02-01',
            '2022-02-03',
            '2022-02-04'
        ],
        'Fold 4': [
            '2022-02-02',
            '2022-02-06',
            '2022-02-07',
            '2022-02-08',
            '2022-02-14',
            '2022-02-15',
            '2022-02-16',
            '2022-02-17',
            '2022-02-18',
            '2022-02-21',
            '2022-03-16',
            '2022-03-17',
            '2022-03-18',
            '2022-03-31',
            '2022-04-01',
            '2022-04-02',
            '2022-04-03',
            '2022-04-04',
            '2022-04-15',
            '2022-04-26',
            '2022-04-27',
            '2022-05-04',
            '2022-05-05',
            '2022-05-06',
            '2022-05-08',
            '2022-05-09',
            '2022-05-10',
            '2022-05-13',
            '2022-05-17'
        ]
    },
    'infrasound': {
        'Fold 1': [
            '2021-01-13',
            '2021-01-14',
            '2021-01-15',
            '2021-01-16',
            '2021-01-24',
            '2021-01-25',
            '2021-01-26',
            '2021-01-27',
            '2021-01-28',
            '2021-01-29',
            '2021-01-30',
            '2021-01-31',
            '2021-02-01',
            '2021-02-02',
            '2021-02-13',
            '2021-02-14',
            '2021-02-24',
            '2021-03-18',
            '2021-03-30',
            '2021-04-01',
            '2021-04-02',
            '2021-04-28',
            '2021-04-29',
            '2021-04-30',
            '2021-05-01',
            '2021-05-02',
            '2021-05-03',
            '2021-05-11',
            '2021-05-12',
            '2021-05-16',
            '2021-05-20',
            '2021-05-23',
            '2021-05-24',
            '2022-01-10',
            '2022-01-11',
            '2022-01-12',
            '2022-01-13',
            '2022-01-14',
            '2022-01-31'
        ],
        'Fold 2': [
            '2022-02-01',
            '2022-02-03',
            '2022-02-04',
            '2022-02-06',
            '2022-02-07',
            '2022-02-08',
        ],
        'Fold 3': [
            '2022-02-02',
            '2022-02-14',
            '2022-02-15',
            '2022-02-16',
            '2022-02-17',
            '2022-02-18',
            '2022-02-21',
            '2022-03-16',
            '2022-03-17',
            '2022-03-18',
            '2022-03-31',
            '2022-04-01',
            '2022-04-02',
            '2022-04-03',
            '2022-04-04',
            '2022-04-15',
            '2022-04-26',
            '2022-04-27',
            '2022-05-04',
            '2022-05-05',
            '2022-05-06',
            '2022-05-08',
            '2022-05-09',
            '2022-05-10',
            '2022-05-13',
            '2022-05-17'

        ]
    }
}
