import torch
from torch.utils.data import DataLoader
from lib.utils.logger import *
from lib.utils.train_utils import Container, Plotter, evaluate_classification
from sklearn.metrics import silhouette_score

logger = logging.getLogger(__name__)


def validate_autoencoder(model, valloader, binary, writer_dict=None):
    val_losses = AverageMeter()
    plotter = Plotter(binary)

    model.eval()
    plot_step = np.random.randint(0, len(valloader), size=10)
    with torch.no_grad():
        val_features, val_labels = Container(), Container()
        for i, sample in enumerate(valloader):

            val_loss, reconstruction, features, target = model.predict(sample)

            val_losses.update(val_loss.item(), target.shape[0])
            if (i == plot_step).any():
                plotter.add_data(reconstruction, target, sample['class_label'], other=None)

            val_features.add(features)
            val_labels.add(sample['class_label'])
    try:
        s_score = silhouette_score(val_features.get(), val_labels.get())
    except:
        s_score = 0

    if writer_dict:
        writer = writer_dict['writer']
        global_steps = writer_dict['global_steps']

        writer.add_scalar(f'validation/{model.criterion.__class__.__name__}', val_losses.avg,
                          global_steps)
        writer.add_scalar(f'validation/Silhouette_Score', s_score, global_steps)
        writer.flush()

    return val_features.get(), val_labels.get(), plotter, val_losses.avg, s_score


def track_on_validation_set(model, loader, iterator):
    model.eval()
    with torch.no_grad():
        try:
            sample = next(iterator)
        except StopIteration:
            iterator = iter(loader)
            sample = next(iterator)

        loss, _, features, _ = model.predict(sample)

        if torch.cuda.is_available():
            features = features.cpu()

        label = sample['class_label'].detach().numpy()
        features = features.detach().numpy()
        try:
            s_score = silhouette_score(features, label)
        except:
            s_score = 0

    return loss, s_score


def train_autoencoder(model, trainloader, valloader, epoch, writer_dict,
                      model_id, log_frequency, eval, args):
    """Train the model for one epoch

    Args:
        model (torch.nn.Module): image segmentation module.
        trainloader (torch.utils.data.DataLoader): dataloader for training set.
        valloader (torch.utils.data.DataLoader): dataloader for validation set.
        criterion (torch.nn.Module): loss function for image segmentation.
        optimizer (torch.optim.Optimizer): optimizer for model parameters.
        device(int): device to train on (gpu or cpu)
        epoch (int): current training epoch.
        writer_dict (dict): dictionary containing tensorboard related objects.
        log_frequency: frequency of logging for tensorboard
        tot_epochs: how many epochs to train in total
    """

    batch_time = AverageMeter()
    data_time = AverageMeter()
    losses = AverageMeter()

    if valloader is not None:
        val_iterator = iter(valloader)
    else:
        val_iterator = None

    end = time.time()
    model_analysis = eval

    torch.autograd.set_detect_anomaly(True)
    for i, sample in enumerate(trainloader):
        data_time.update(time.time() - end)

        model.train()
        loss = model.fit(sample)
        losses.update(loss.item(), args.batch)

        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()

        if i % log_frequency == 0:
            val_loss, s_score = track_on_validation_set(model, loader=valloader, iterator=val_iterator)

            logger.info(
                get_log_message(epoch, i, len(trainloader), batch_time=batch_time,
                                speed=args.batch / batch_time.val, data_time=data_time,
                                train_loss=losses.val, val_loss=val_loss)
            )

            if writer_dict:
                writer = writer_dict['writer']
                global_steps = writer_dict['global_steps']

                writer.add_scalar(f'train_loss/{model.criterion.__class__.__name__}', losses.val, global_steps)
                writer.add_scalar(f'val_loss/{model.criterion.__class__.__name__}', val_loss, global_steps)
                writer.add_scalar(f's_score/Silhouette Score', s_score, global_steps)
                writer_dict['global_steps'] = global_steps + 1
                writer.flush()

    # plot reconstruction, confusion matrices and latent space
    if model_analysis:

        model.eval()
        with torch.no_grad():
            # calculate validation loss
            eval_trainloader = DataLoader(trainloader.dataset, batch_size=int(args.batch), shuffle=False,
                                          pin_memory=True, drop_last=True)
            train_features, train_labels = Container(), Container()
            for i, sample in enumerate(eval_trainloader):
                _, reconstruction, features, _ = model.predict(sample)

                train_features.add(features)
                train_labels.add(sample['class_label'])

        val_features, val_labels, plotter, val_loss, s_score = validate_autoencoder(
            model, valloader, binary=args.binary, writer_dict=None
        )
        plotter.plot(epoch+1, writer_dict)
        evaluate_classification(
            train_features.get(), val_features, train_labels.get(), val_labels, epoch + 1, model_id, writer_dict, args
        )
