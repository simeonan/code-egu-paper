import numpy as np
import pandas as pd
import scipy.signal as scp
import os

from torch.utils.data import Dataset, DataLoader
from lib.utils.variables import *
from torch.utils.data import WeightedRandomSampler


def get_data_loaders(args, k, batch_size=64, shuffle=True, feature_ext=False):
    if feature_ext:
        aug = False
    else:
        aug = args.aug
    data_train = WindowDataset(data_type=args.data_type, ts_len=int(args.ts_len), split="train", final=args.final,
                               frequency_input=args.frequency_input, spectral_length=args.spectral_length,
                               binary=args.binary, fold=k, batch_dist=args.batch_dist, aug=aug, feature_ext=feature_ext
                               )
    if args.batch_dist and not feature_ext:
        w = data_train.weights
        weighted_sampler = WeightedRandomSampler(w, data_train.ds_len, replacement=True)
        trainloader = DataLoader(data_train, batch_size=int(batch_size), sampler=weighted_sampler,
                                 pin_memory=True, drop_last=True)
    else:
        trainloader = DataLoader(data_train, batch_size=int(batch_size), shuffle=shuffle,
                                 pin_memory=True, drop_last=True)

    data_validate = WindowDataset(data_type=args.data_type, ts_len=int(args.ts_len), split="validate", final=args.final,
                                  frequency_input=args.frequency_input, spectral_length=args.spectral_length,
                                  binary=args.binary, fold=k)
    valloader = DataLoader(data_validate, batch_size=int(batch_size), shuffle=shuffle,
                           pin_memory=True, drop_last=True)

    dataset_args = data_train.dataset_args

    return trainloader, valloader, dataset_args


def get_fft(ts, n, fs_length):
    fseries = np.abs(np.fft.fft(ts, n=n))
    fseries = fseries[:fs_length]

    max = np.max(fseries)
    min = np.min(fseries)
    fseries = (fseries - min) / (max - min)
    return fseries


class WindowDataset(Dataset):
    def __init__(self,
                 data_type='seismic',
                 ts_len=2000,
                 split="train",
                 final=False,
                 frequency_input=False,
                 spectral_length=200,
                 binary=False,
                 batch_dist=0.,
                 aug=0.,
                 fold=0,
                 feature_ext=False
                 ):
        torch.manual_seed(1)
        np.random.seed(seed=1)
        # LOAD DATA
        self.data_type = data_type
        self.sensors = ['CT', 'CS', 'WE', 'NE', 'SE']
        directory = DATA_DIRECTORY + 'seismic_windows/'

        self.spr = 200
        NF = self.spr / 2
        self.output_pts = spectral_length
        upper_freq = 10
        self.n = int(np.floor(2 * NF * self.output_pts / (upper_freq + 10)))
        self.f_factor = int(ts_len/self.output_pts)
        self.fft_length = self.output_pts
        self.aug = aug

        if aug and not feature_ext:
            self.filename = 'f1-10_w10_o5_add2.csv'
        else:
            self.filename = 'f1-10_w10_o5.csv'

        data = pd.read_csv(directory + self.filename).drop(columns='Unnamed: 0')
        self.datafile = data

        if binary:
            self.datafile['label'] = self.datafile['label'].apply(lambda x: 0 if (x == 2 or x == 0) else x)

        # SPLIT
        self.datafile['date'] = self.datafile['Event_id'].str[0:4] + '-' \
                                + self.datafile['Event_id'].str[4:6] + '-' \
                                + self.datafile['Event_id'].str[6:8]

        CV_FOLDS = list(SPLITS[data_type].values())
        self.datafile['cv_fold'] = self.datafile['date'].apply(
            lambda x: 'test' if x in CV_FOLDS[-1] else 'validate' if x in CV_FOLDS[fold] else 'train'
        )

        if final:
            if split == 'validate':
                self.data = self.datafile.loc[(self.datafile['cv_fold'] == 'test'), :]
            else:
                self.data = self.datafile.loc[(self.datafile['cv_fold'] == 'train') + (self.datafile['cv_fold'] == 'validate'), :]
        else:
            if split == 'validate':
                self.data = self.datafile.loc[(self.datafile['cv_fold'] == 'validate'), :]
            else:
                self.data = self.datafile.loc[(self.datafile['cv_fold'] == 'train'), :]

        self.data = self.data.reset_index()
        self.data = self.data.drop(columns=['index'])

        self.data['sample'] = self.data['sample'].apply(
            lambda x: np.array([float(d) for d in x.replace('[', '').replace(']', '').split(',')])
        )

        # STATISTICS OF USED DATASET
        if binary:
            n_classes = 2
        else:
            n_classes = 3

        n_events = []
        for _class in range(n_classes):
            n_events.append(sum((self.data['label'] == _class).to_numpy()))
        n_total = sum(n_events)
        label_map = {0: 'noise', 1: 'avalanche', 2: 'earthquake'}
        label_names = [label_map[i] for i in range(n_classes)]
        fractions = [n_event/n_total for n_event in n_events]
        self.stats = {
            **{
                'total': n_total
            },
            **{
                event: n for event, n in zip(label_names, fractions)
            }
        }
        print(self.stats)
        if batch_dist:
            av = batch_dist
            w_r = n_events[0] / n_events[1] * av / (1 - av)
            self.class_weights = torch.tensor([1, w_r], requires_grad=False)
            self.class_weights = self.class_weights.float()
            self.weights = torch.tensor([
                self.class_weights[int(label)]
                for label in self.data['label']
            ])
        else:
            self.class_weights = torch.tensor(
                [1 - n_event / n_total for n_event in n_events], requires_grad=False)
            self.class_weights = self.class_weights.float()
            self.weights = torch.tensor([
                self.class_weights[label]
                for label in self.data['label']
            ])

        # NORMALIZATION
        self.dataset_ids = [i for i in range(len(self.data))]
        self.data['dataset_id'] = pd.DataFrame(self.dataset_ids)

        # MORE VARIABLES
        self.f_input = frequency_input
        self.ts_len = ts_len
        self.ds_len = len(self.data)

        self.dataset_args = {
            **self.stats,
            **{
                'tdim': self.ts_len,
                'fdim': self.fft_length if self.f_input else 0
            }
        }

    def __len__(self):
        return self.ds_len

    def __getitem__(self, idx):
        row = self.data.loc[idx, :]
        dataset_id = np.array(row['dataset_id'])
        event_id = row['Event_id']
        array_ts = row['sample'][:-1]
        if self.aug:
            random_offset = np.random.uniform(low=0.0, high=2.0)
            start = int(random_offset * self.spr)
            end = int(start + self.ts_len)
        else:
            start = int(0 * self.spr)
            end = int(start + self.ts_len)
        array_ts = array_ts[start:end]
        label = torch.tensor(row['label'])
        av_score = torch.tensor(row['av_score'])
        eq_score = torch.tensor(row['eq_score'])

        abs = np.array(np.abs(array_ts))
        absmax = np.max(abs, axis=-1)

        ts = torch.tensor(array_ts).unsqueeze(0)
        raw_ts = torch.clone(ts)
        temporal = ts/absmax

        if self.f_input:
            fseries = get_fft(array_ts, self.n, fs_length=self.output_pts)
            spectral = torch.tensor(fseries).unsqueeze(0)
        else:
            spectral = torch.zeros((1, self.output_pts))

        sample = {
            'temporal input': temporal.float(),
            'spectral input': spectral.float(),
            'class_label': label.float(),
            'av_score': av_score.float(),
            'eq_score': eq_score.float(),
            'array input': array_ts,
            'raw input': raw_ts.float(),
            'id': dataset_id,
            'event': [event_id],
            'ds_len': torch.tensor(self.ds_len)
        }
        return sample
