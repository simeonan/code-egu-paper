# Autoencoder-based feature extraction for the automatic detection of snow avalanches in seismic data

## Description
This repository provides the functionalities to optimize and test the models presented 
in this [publication](https://doi.org/10.5194/gmd-2024-76) of the same name.

## Installation and Overview
To run and test the code the user is asked to download the repository and install all necessary requirements 
listed in the requirements.txt file. 
```
pip install -r requirements.txt
```
Upon successful installation the root directory path needs to be changed in lib/utils/variables.py. All set up, one can start optimize different model parameters (code/cross_validation.py), infer the features on the test set (code/inference.py), 
optimize a random forest classifier for a given set of features (code/classifier_optimization.py) and test the classification (code/feature_classification.py).
The implementation of the spectral autoencoder (SAE) and the temporal autoencoder (TAE) are found in the models directory. 

## Usage
To preform cross-validation run: 
```
python code/run_script.py --model sae --type cross-validation --ablation model_parameters
```
To preform inference and extract autoencoder features run: 
```
python code/run_script.py --model sae --type inference
```
To optimize a random forest model on the autoencoder features run: 
```
python code/classifier_optimization.py --model sae
```
To test the classification: 
```
python code/feature_classification.py --model sae
```

## Support
In case of any troubles or question we are happy to halp you out. Contact us over: andri.simen@slf.ch

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
Distributed under the GNU GPL v3. See LICENSE.txt for more information.
