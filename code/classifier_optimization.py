import pandas as pd
import argparse
import os
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GroupKFold
from sklearn.metrics import make_scorer, balanced_accuracy_score, f1_score
from sklearn.model_selection import RandomizedSearchCV

from lib.utils.variables import *

import warnings
import time
warnings.filterwarnings('ignore')

CV_FOLDS = list(SPLITS['seismic'].values())


from joblib import parallel_backend


def parse_args():
    parser = argparse.ArgumentParser(description='Training mode')
    # training
    parser.add_argument('--model',
                        help='optimize over features of this model',
                        default='tae',
                        type=str)
    args = parser.parse_args()
    return args


def optimize_random_forest(model_name):
    starttime = time.time()

    project_dir = PROJECT_DIRECTORY
    data_dir = DATA_DIRECTORY

    with parallel_backend('threading', n_jobs=-1):

        path_lib = {
            'data_input': data_dir + 'features_archive/',
            'data_output': project_dir + 'results/random_forest_optimization/'
        }

        if not os.path.exists(path_lib['data_output']):
            os.makedirs(path_lib['data_output'])

        data = pd.read_csv(path_lib['data_input'] + model_name + '_train_features.csv').drop(columns='Unnamed: 0')
        data['label'] = data['label'].apply(lambda x: 0 if (x == 2 or x == 0) else 1)
        feature_columns = [name for name in data.columns if name not in META_COLUMNS]

        cv_folds = CV_FOLDS[:-1]
        data['date'] = data['Event_id'].str[0:4] + '-' + data['Event_id'].str[4:6] + '-' + data['Event_id'].str[6:8]
        data['cv_fold'] = data['date'].apply(
            lambda x: 1 if x in cv_folds[0] else 2 if x in cv_folds[1] else 3 if x in cv_folds[2] else 0)
        data = data.loc[data['cv_fold'] != 0]
        groups = list(data['cv_fold'])
        number_groups = len(cv_folds)

        gkf = GroupKFold(n_splits=number_groups)

        Xtrn, Ytrn = data[feature_columns], data['label']
        cv_method = gkf.split(Xtrn, Ytrn, groups=groups)

        random_grid = {
            'n_estimators': [32, 64, 128, 256, 512, 1024],
            'criterion': ['gini'],
            'max_depth': [4, 8, 16, 32],
            'max_features': [None, 'sqrt', 'log2'],
            'bootstrap': [True],
            'class_weight': ['balanced'],
            'max_samples': [0.1, 0.2, 0.4, 0.6, 0.8, None]
        }

        f1 = {'f1-binary': make_scorer(f1_score, average='binary'), 'f1-macro': make_scorer(f1_score, average='macro'),
              'balanced_accuracy': make_scorer(balanced_accuracy_score)}
        # %%
        rf = RandomForestClassifier(random_state=42, class_weight='balanced')
        rf_random = RandomizedSearchCV(estimator=rf, param_distributions=random_grid,
                                       n_iter=200, scoring=f1, refit=False, n_jobs=7, pre_dispatch=7,
                                       cv=cv_method, verbose=2, random_state=42,
                                       return_train_score=True)
        rf_random.fit(Xtrn, Ytrn)

        GS_dry = pd.DataFrame(rf_random.cv_results_)
        GS_dry.to_csv(path_lib['data_output'] + 'cv_results_' + model_name + '.csv')
        print('runtime: ', time.time()-starttime)


def main():
    parser = argparse.ArgumentParser(description='Training mode')
    # training
    parser.add_argument('--model',
                        help='optimize over features of this model',
                        default='tae',
                        type=str)
    args = parser.parse_args()
    optimize_random_forest(args.model)


if __name__ == "__main__":
    main()
