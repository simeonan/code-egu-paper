import argparse
import subprocess

from models.ablations import *


def get_inference_command(base_command, model='tae'):
    if model == 'sae':
        selected_model = selected_sae
    else:
        selected_model = selected_tae

    parameters = list(selected_model.columns)
    cmd_append = ''
    for parameter in parameters:
        name = ' --' + parameter + ' '
        value = selected_model.loc[0, parameter]
        cmd = name + str(value)
        cmd_append += cmd

    return [base_command + cmd_append]


def get_cv_commands(base_command, model='tae', ablation_type='model_parameters'):
    if model == 'sae':
        if ablation_type == 'model_parameters':
            ablation = sae_model_parameters_ablation
        elif ablation_type == 'data_parameters':
            ablation = sae_data_parameters_ablation
        else:
            ablation = sae_training_parameters_ablation
    else:
        if ablation_type == 'model_parameters':
            ablation = tae_model_parameters_ablation
        else:
            ablation = tae_training_parameters_ablation

    commands = []

    for i in range(len(ablation)):
        parameters = list(ablation.columns)
        cmd_append = ''
        for parameter in parameters:
            name = ' --' + parameter + ' '
            value = ablation.loc[i, parameter]
            cmd = name + str(value)
            cmd_append += cmd

        command = base_command + cmd_append

        commands.append(command)

    return commands


def parse_args():
    parser = argparse.ArgumentParser(description='Training mode')
    # training
    parser.add_argument('--model',
                        help='which model to use',
                        default='tae',
                        type=str)
    parser.add_argument('--type',
                        help='which model to use',
                        default='cross-validation',
                        type=str)
    parser.add_argument('--ablation',
                        help='which model to use',
                        default='model_parameters',
                        type=str)

    args = parser.parse_args()

    return args


def main():
    args = parse_args()

    if args.type == 'cross-validation':
        base_command = 'python code/cross_validation.py --model ' + args.model + ' --binary 1 --kfold_cv 1'
        if args.model == 'sae':
            base_command += ' --frequency_input 1'
        cmds = get_cv_commands(base_command, model=args.model, ablation_type=args.ablation)
    elif args.type == 'inference':
        base_command = 'python code/inference.py --model ' + args.model + ' --binary 1 --final 1 --kfold_cv 1'
        if args.model == 'sae':
            base_command += ' --frequency_input 1'
        cmds = get_inference_command(base_command, model=args.model)
    else:
        cmds = None

    for i, cmd in enumerate(cmds):
        print("\nRunning command " + str(i+1) + " of " + str(len(cmds)) + ' :')
        print(cmd)
        process = subprocess.run(cmd, shell=True, capture_output=True)
        output = process.stderr.decode()
        print(output)


if __name__ == "__main__":
    main()
