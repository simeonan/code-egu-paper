import argparse
import pandas as pd
import os
import seaborn as sns
import matplotlib.pyplot as plt
from lib.utils.variables import *
from models.Random_Forests import get_random_forest
from lib.utils.test_utils import save_analysis, ensemble_predictions, get_features, plot_expert_ratings_over_output_probabilities
from sklearn.metrics import calinski_harabasz_score, silhouette_score


def plot_and_save_results(predictions, clustering_metrics, settings, save_dir):
    model_id = settings['model_id']
    predictions.to_csv(save_dir + 'predictions.csv')
    save_analysis(save_dir, model_id, y_true=list(predictions['Ytrue']), y_hat=list(predictions['Yhat']),
                  clustering_metrics=clustering_metrics, split='val', binary=settings['binary'])
    n_win = 2

    df_over_sensors, df_final = ensemble_predictions(predictions, n_win=n_win)
    aggregation_dfs = {
        'sens': df_over_sensors,
        'fin': df_final
    }

    plot_expert_ratings_over_output_probabilities(df_over_sensors, save_dir, model_name=model_id,
                                                  filename='expert_scores_analysis_sens')

    aggregation_methods = ['mean_maj', 'consecutive_' + str(n_win)]
    for name, aggregation_df in aggregation_dfs.items():
        aggregation_df.to_csv(save_dir + name + '_predictions.csv')
        for agg_method in aggregation_methods:
            if 'consecutive' in agg_method and name == 'sens':
                continue
            save_analysis(
                save_dir,
                model_id,
                y_true=list(aggregation_df['Ytrue']),
                y_hat=list(aggregation_df[agg_method]),
                split=name + '_' + agg_method,
                binary=settings['binary']
            )


def plot_latentspace(features, labels, sorted_features, sorted_importance, setting, save_dir, filename, eq=False):
    names = sorted_features[:3]
    importances = sorted_importance[:3]
    if 'engineered' in setting['model_id']:
        title = 'Seismic Attributes'
        mapper = {name: name + ' (' + str(importances[i][-1]) + ')' for i, name in enumerate(names)}
    elif 'tae' in setting['model_id']:
        title = 'TAE Features'
        mapper = {name: 'F' + name[1:] + ' (' + str(importances[i][-1]) + ')' for i, name in enumerate(names)}
    elif 'sae' in setting['model_id']:
        title = 'SAE Features'
        mapper = {name: 'F' + name[1:] + ' (' + str(importances[i][-1]) + ')' for i, name in enumerate(names)}
    else:
        title = ''
        mapper = None

    features_df = features[names].rename(columns=mapper)
    labels_df = pd.DataFrame(labels, columns=['label'])
    data_pp = pd.concat((features_df, labels_df), axis=1)

    rc = {'figure.figsize': (5, 5), 'figure.dpi': 600, 'font.size': 16, 'axes.labelsize': 16,
          'axes.titlesize': 16}
    sns.set_theme(style="ticks", color_codes=True, rc=rc)

    palette = sns.xkcd_palette(['grey', 'blue'] if not eq else ['grey', 'blue', 'orange'])
    grid = sns.pairplot(data_pp, hue='label', palette=palette)

    grid.fig.suptitle(title, y=1.01)
    grid._legend.remove()
    plt.savefig(save_dir + filename + '.png', bbox_inches='tight')
    plt.close()


def classify_features(model_name):
    settings = {
        'data_type': 'seismic',
        'binary': True,
        'latex_str': False
    }
    project_dir = PROJECT_DIRECTORY
    features_dir = DATA_DIRECTORY + 'features_archive/'

    settings['model_id'] = model_name

    save_dir = project_dir + 'results/classification/' + model_name + '/'
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)

    features = get_features(features_dir, model=model_name)
    feature_columns = features['names']

    random_forest, _ = get_random_forest(model=model_name)
    random_forest.fit(features['train'][feature_columns], features['train']['label'])

    ### Plot Latent Space ###
    importances = list(random_forest.feature_importances_)
    feature_importances = [(feature, round(importance, 4)) for feature, importance in
                               zip(list(features['train'][feature_columns]), importances)]
    feature_importances = sorted(feature_importances, key=lambda x: x[1], reverse=True)
    sorted_features = [importance[0] for importance in feature_importances]

    filename = 'latent_space'
    latentspace_labels = features['test'][['av_score', 'eq_score']].apply(
        lambda x: 1 if (x[0] >= 2.) else 2 if (x[1] >= 2.) else 0, axis=1
    )
    plot_latentspace(features['test'][feature_columns], latentspace_labels,
                     sorted_features, feature_importances,
                     settings, save_dir, filename, eq=True)

    true_labels = features['test']['label']
    y_hat_val = random_forest.predict(features['test'][feature_columns])
    y_hat_val_prob = random_forest.predict_proba(features['test'][feature_columns])

    s_score = silhouette_score(features['test'][feature_columns], true_labels)
    ch_score = calinski_harabasz_score(features['test'][feature_columns], true_labels)
    clustering_metrics = {
        'silhouette': s_score,
        'chi': ch_score
    }

    probabilities = ['P_noise', 'P_av']
    p_agg = dict(zip(probabilities, [y_hat_val_prob[:, i] for i in range(len(probabilities))]))

    pred_df = pd.DataFrame(
        {
            **{
                'Event_id': features['test']['Event_id'],
                'Component': features['test']['Component'],
                'initial_time': features['test']['initial_time'],
                'av_score': features['test']['av_score'],
                'eq_score': features['test']['eq_score'],
                'Ytrue': true_labels,
                'Yhat': y_hat_val
            },
            **p_agg
        }
    )
    plot_and_save_results(pred_df, clustering_metrics, settings, save_dir)


def main():
    parser = argparse.ArgumentParser(description='Classify Features')
    parser.add_argument('--model',
                        help='which model features to classify',
                        default='engineered',
                        type=str)

    args = parser.parse_args()

    classify_features(args.model)


if __name__ == "__main__":
    main()
