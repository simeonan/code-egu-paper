import os
import time
import torch.optim as optim

from lib.core.function import train_autoencoder
from lib.utils.test_utils import extract_encoder_features
from lib.dataset.dataloader import get_data_loaders
from lib.utils.model_utils import *
from lib.utils.logger import *
from lib.utils.train_utils import *


def main():
    torch.manual_seed(1)
    time_str = time.strftime('%Y-%m-%d-%H-%M')

    args = parse_args()
    dirs = get_dirs(args, time_str)

    logger, writer = create_logger(out_dir=dirs['train_log_dir'],
                                   name=args.model + "_" + args.loss,
                                   phase='train',
                                   create_tf_logs=True)
    if writer:
        writer_dict = {'writer': writer, 'global_steps': 0}
    else:
        writer_dict = None

    trainloader, valloader, dataset_args = get_data_loaders(
        args, k=args.fold, batch_size=args.batch, shuffle=True
    )

    if 'dict' in args.__class__.__name__ or 'DotDict' in args.__class__.__name__:
        args_dict = args
    else:
        args_dict = vars(args)

    model_dict = {'model_id': dirs['model_id']}
    run_args = DotDict({**model_dict, **args_dict, **{'machine': dirs['machine']}, **dataset_args})

    model, n_param, n_modules, on_gpu = load_model(run_args, dirs['model_id'], pretrained=False, device=dirs['device'])

    model_info = 'Model: {model}\t' \
                 'Learnable Weigths: {n_weights}\t' \
                 'Training on GPU: {on_gpu}\t' \
        .format(model=model.__class__.__name__, n_weights=n_param, on_gpu=on_gpu)
    logger.info(model_info)

    args_to_save = {
        **model_dict,
        **{'n_weights': n_param, 'n_modules': n_modules, 'on_gpu': on_gpu},
        **args_dict,
        **{'machine': dirs['machine']},
        **dataset_args
    }
    save_args(args_to_save, dirs['project_dir'])

    start_epoch = 0

    for epoch in range(start_epoch, start_epoch + run_args.epochs):
        eval = (epoch + 1) % 5 == 0
        train_autoencoder(model, trainloader, valloader, epoch, model_id=dirs['model_id'], writer_dict=writer_dict,
                          log_frequency=500, eval=eval, args=run_args)

        if epoch+1 == run_args.extract_features:
            splits = ['train', 'validate']

            model_args = {**{'model_id': dirs['model_id']}, **vars(args)}
            model_args = DotDict(model_args)
            for split in splits:
                extract_encoder_features(model, model_args, split=split, data_dir=dirs['data_dir'])

        try:
            torch.save(
                {
                    'epoch': epoch,
                    'model_state_dict': model.state_dict(),
                    'optimizer_state_dict': model.optimizer.state_dict()
                }, dirs['model_log_dir'] + 'model_ep' + str(epoch) + '.pth'
            )
        except:
            continue
    writer.flush()


if __name__ == "__main__":
    main()
