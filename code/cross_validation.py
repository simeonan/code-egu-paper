import os
import argparse
import pandas as pd
from sklearn.metrics import calinski_harabasz_score

from lib.dataset.dataloader import get_data_loaders
from lib.utils.model_utils import *
from lib.utils.logger import *
from lib.utils.train_utils import *
from lib.utils.variables import *
from lib.core.function import train_autoencoder, validate_autoencoder
from models.ablations import *

CV_FOLDS = list(SPLITS['seismic'].values())


def main():
    args = parse_args()

    time_str = time.strftime('%Y-%m-%d-%H-%M')
    dirs = get_dirs(args, time_str)

    cv_folds = [i for i in range(len(CV_FOLDS)-1)]
    metrics = ['s_score', 'vrc_score', 'mse_loss']
    model_evaluation = {
        **{'model': dirs['model_id']},
        **{
            metric + '.fold' + str(fold): [] for fold in cv_folds for metric in metrics
        }
    }
    for cv_fold in cv_folds:
        train_log_dir = dirs['train_log_dir']
        model_id = dirs['model_id'] + '_f' + str(cv_fold)

        logger, writer = create_logger(out_dir=train_log_dir,
                                       name=args.model + "_" + args.loss,
                                       phase='train',
                                       create_tf_logs=True,
                                       fold=cv_fold)
        if writer:
            writer_dict = {'writer': writer, 'global_steps': 0}
        else:
            writer_dict = None

        trainloader, valloader, dataset_args = get_data_loaders(
            args, k=cv_fold, batch_size=args.batch, shuffle=True
        )

        if 'dict' in args.__class__.__name__ or 'DotDict' in args.__class__.__name__:
            args_dict = args
        else:
            args_dict = vars(args)

        model_dict = {'model_id': model_id}
        run_args = DotDict({**model_dict, **args_dict, **dataset_args})

        model, n_param, n_modules, on_gpu = load_model(run_args, model_id, pretrained=False, device=dirs['device'])
        model_info = 'Model: {model}\t' \
                     'Learnable Weigths: {n_weights}\t' \
                     'Training on GPU: {on_gpu}\t' \
            .format(model=model.__class__.__name__, n_weights=n_param, on_gpu=on_gpu)
        logger.info(model_info)

        args_to_save = {
            **model_dict,
            **{'n_weights': n_param, 'n_modules': n_modules, 'on_gpu': on_gpu},
            **args_dict,
            **dataset_args
        }
        save_args(args_to_save, train_log_dir, machine=dirs['machine'])

        for epoch in range(0, run_args.epochs):
            eval = (epoch+1) % 10 == 0 or epoch == 0
            #######################
            #    TRAINING STEP    #
            #######################
            train_autoencoder(model, trainloader, valloader, epoch=epoch, writer_dict=writer_dict,
                              model_id=model_id, log_frequency=500, eval=False, args=args)

            #######################
            #   EVALUATION STEP   #
            #######################
            val_features, val_labels, plotter, val_loss, s_score = validate_autoencoder(
                model, valloader, binary=args.binary, writer_dict=None
            )
            vrc = calinski_harabasz_score(val_features, val_labels)

            model_evaluation['mse_loss.fold' + str(cv_fold)].append(val_loss)
            model_evaluation['s_score.fold' + str(cv_fold)].append(s_score)
            model_evaluation['vrc_score.fold' + str(cv_fold)].append(vrc)

            if eval:
                plotter.plot(epoch=epoch+1, writer_dict=writer_dict, cv_fold=cv_fold)
                latent_space_data = get_latent_space(model, val_features, val_labels)
                plot_latentspace(latent_space_data, writer_dict, epoch+1, cv_fold=cv_fold)

        writer.flush()

    metrics = ['s_score', 'vrc_score', 'mse_loss']
    cv_dir = dirs['project_dir'] + 'results/cross_validation/' + args.model + '/'
    if not os.path.exists(cv_dir):
        os.makedirs(cv_dir)
    results = pd.DataFrame.from_dict(model_evaluation, orient='columns')
    for metric in metrics:
        for fold in cv_folds:
            mean_window = 4 if args.epochs >= 4 else args.epochs
            results[metric + '.fold' + str(fold)] = results[metric + '.fold' + str(fold)].rolling(mean_window).mean()
        results[metric + '_mean'] = results.apply(
            lambda x: np.mean([x[metric + '.fold' + str(fold)] for fold in cv_folds]), axis=1
        )
        results[metric + '_std'] = results.apply(
            lambda x: np.std([x[metric + '.fold' + str(fold)] for fold in cv_folds]), axis=1
        )
        results[metric + '_rank'] = results[metric + '_mean'].rank(ascending=False if metric != 'mse_loss' else True)
        results = results.drop(columns=[metric + '.fold' + str(fold) for fold in cv_folds])

    results.to_csv(cv_dir + 'cv_results_' + dirs['model_id'] + '.csv')


if __name__ == "__main__":
    main()
